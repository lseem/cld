# cld version
VERSION = 0.1

# paths
PREFIX = /usr

# includes and libs

# flags
CFLAGS = -g -I/usr/include/libxml2
LDFLAGS = -lm -lxml2

# compiler and linker
CC = gcc
