# cld - command line dictionary

include config.mk

SRC = cld.c
OBJ = ${SRC:.c=.o}

all: options cld

options:
	@echo cld build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.c.o:
	@echo CC $<
	@${CC} -c ${CFLAGS} $<

${OBJ}: config.mk

cld: ${OBJ}
	@echo CC -o $@
	@${CC} -o $@ ${OBJ} ${LDFLAGS}

clean:
	@echo cleaning
	@rm -f cld ${OBJ} cld-${VERSION}.tar.gz

dist: clean
	@echo creating dist tarball
	@mkdir -p cld-${VERSION}
	@cp -R ${SRC} Makefile config.mk TODO cld-${VERSION}
	@tar -cf cld-${VERSION}.tar cld-${VERSION}
	@gzip cld-${VERSION}.tar
	@rm -rf cld-${VERSION}

install: all
	@echo installing executable file to ${DESTDIR}${PREFIX}/bin
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@cp -f cld ${DESTDIR}${PREFIX}/bin
	@chmod 755 ${DESTDIR}${PREFIX}/bin/cld

uninstall:
	@echo removing executable file from ${DESTDIR}${PREFIX}/bin
	@rm -f ${DESTDIR}${PREFIX}/bin/cld
