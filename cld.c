#include <stdlib.h> /*ANSI C exit() */
#include <libxml/parser.h>
#include <libxml/tree.h>

void init(int argc, char **argv);
void search(char *word);

xmlDocPtr dict;
xmlNodePtr cur;
xmlChar *format;

int
main(int argc, char **argv)
{	

	dict = xmlParseFile("dict.xml");
	cur = xmlDocGetRootElement(dict);
	format = xmlGetProp(cur, "format");	
	
	init(argc, argv);

	search(argv[1]);

	exit(EXIT_SUCCESS);
}

void
init(int argc, char **argv)
{
	if (argc != 2)
	{
		fprintf(stderr, "Usage: %s <word>\n Ensure that the dictionary file is in the working directory and is named dict.xml.\n", argv[0]);
		xmlFreeDoc(dict);
		exit(EXIT_FAILURE);
	}

	if (dict == NULL)
	{
		fprintf(stderr, "dict.xml not parsed successfully\n");
		xmlFreeDoc(dict);
		exit(EXIT_FAILURE);
	}

	if (cur == NULL)
	{
		fprintf(stderr, "empty document\n");
		xmlFreeDoc(dict);
		exit(EXIT_FAILURE);
	}

	if (xmlStrcmp(cur->name, (const xmlChar *) "xdxf"))
	{
		fprintf(stderr, "root node is not xdxf\n");
		xmlFreeDoc(dict);
		exit(EXIT_FAILURE);
	}

	if (xmlStrcmp(format, (const xmlChar *) "logical")) 
	{
		fprintf(stderr, "'%s'\n", format);
		fprintf(stderr, "xdxf dictionary is not of logical format\n");
		exit(EXIT_FAILURE);
	}
	
	printf("Init finished. No errors reported.\n");
}

void
search(char *word)
{
	/* set cursor to root element */	
	cur = xmlDocGetRootElement(dict);

	/* get the first child of the root element */
	cur = cur->children;
	/* while the cursor is not "lexicon" AND not NULL, go to the next element */
	while((xmlStrcmp(cur->name, (const xmlChar *)"lexicon")) && (cur != NULL))
		cur = cur->next;
	
	if (xmlStrcmp(cur->name, (const xmlChar *)"lexicon"))
	{
		fprintf(stderr, "<lexicon> not found.");
		exit(EXIT_FAILURE);
	}
	
	printf("lexicon\n");

	int found = 0;
	int c = 1;
	while(found == 0)
	{
		/* get the next child of <lexicon> */
		for (int i = 0; i < c; i++)
			cur = cur->children;
		/* while the cursor is not "ar" AND not NULL, go to the next element */
		while( (cur != NULL) && (xmlStrcmp(cur->name, (const xmlChar *)"ar")) )
			cur = cur->next;
		
		if (cur == NULL)
		{
			fprintf(stderr, "<ar> not found.");
			found = -1;	
		}
		else
		{
		
			printf("\tar\n");
	
			/* get the first child of <ar> */
			cur = cur->children;
			/* while the cursor is not "k" AND not NULL, go to the next element */
			while((xmlStrcmp(cur->name, (const xmlChar *)"k")) && (cur != NULL))
				cur = cur->next;
		
			if (xmlStrcmp(cur->name, (const xmlChar *)"k"))
			{
				fprintf(stderr, "<k> not found.\n");
				exit(EXIT_FAILURE);
			}
		
			printf("\t\tk\n");
	
			/* if the contents of <k> is argv[1] */
	
			xmlChar *key = xmlNodeListGetString(dict, cur->children, 1);
			if (!xmlStrcmp(key, (const xmlChar *)word))
			{
				printf("we found your word %s which matches with %s in the dictionary\n", word, key);
				found = 1;
			}
		
			/* go back two parents */
			cur = cur->parent;
			cur = cur->parent;
			/* we are back at lexicon */
			/* increase the child increment by one so next time we go the the child after this failed one */
			c++;
		}
	}

	printf("search complete\n");	
}
